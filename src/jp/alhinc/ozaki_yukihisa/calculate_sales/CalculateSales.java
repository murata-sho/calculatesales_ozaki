package jp.alhinc.ozaki_yukihisa.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;

public class CalculateSales{
	public static void main (String[] args){
        Map <String, String> branchMap = new LinkedHashMap <String, String>();
        Map <String, Long> salesMap = new LinkedHashMap <String, Long>();
 //支店定義ファイル読み込み
        try{
        	BufferedReader branchBr = null;
        	try{
        		File branchFile = new File(args[0], "branch.lst");
        		if(!branchFile.exists()){
        			System.out.println("支店定義ファイルが存在しません");
        			return;
        		}
        		branchBr = new BufferedReader(new FileReader(branchFile));
        		String branchLine;
        		while((branchLine = branchBr.readLine()) != null){
        			String[] branchSplit = branchLine.split(", ");
        			if(!branchSplit[0].matches("^\\d{3}$") || branchSplit.length != 2){
        				System.out.println("支店定義ファイルのフォーマットが不正です");
        				return;
        			}
        			branchMap.put(branchSplit[0], branchSplit[1]);
        		}
        	}finally{
        		branchBr.close();
        	}
//集計       	
        	FilenameFilter rcdFilter = new FilenameFilter(){
        		public boolean accept(File file, String str){
        			return str.matches("^\\d{8}.rcd$") && new File(file, str).isFile();
        		}
        	};
        	BufferedReader salesBr = null;
        	try {
        		File[] salesFile = new File(args[0]).listFiles(rcdFilter);
        		for(int i = 0 ; i < salesFile.length ; i ++){
        			salesBr = new BufferedReader(new FileReader(salesFile[i]));
        			
        			String minimumStr = salesFile[0].getName().substring(0, 8);
        			int minimum = Integer.parseInt(minimumStr);
        			
        			String maximumstr = salesFile[salesFile.length - 1].getName().substring(0, 8);
        			int maximum = Integer.parseInt(maximumstr);
        			if(minimum + salesFile.length == maximum){
        				System.out.println("売上ファイル名が連番になっていません");
        				return;
        			}	
        			String codeLine = salesBr.readLine();
        			if(!branchMap.containsKey(codeLine)){
        				System.out.println(salesFile[i].getName() + "のコードが不正です");
        				return;
        			}
        			String salesLine = salesBr.readLine();
        			String emptyLine = salesBr.readLine();
        			if(emptyLine != null){
        				System.out.println(salesFile[i].getName() + "のフォーマットが不正です");
        				return;
        			}
        			long Total = Long.parseLong(salesLine);
        			if(salesMap.containsKey(codeLine)){
        				Total = Total + salesMap.get(codeLine);
        			}
        			if(String.valueOf(Total).length() > 10){
        				System.out.println("合計金額が10桁を超えました");
        				return;
        			}
        			salesMap.put(codeLine, Total);
        		}
        	}finally{
        		salesBr.close();
        	}
//集計結果出力
        	PrintWriter branchOutPw = null;
        	try{
        		File branchOutFile = new File(args[0], "branch.out");
        		branchOutPw = new PrintWriter(new BufferedWriter(new FileWriter(branchOutFile)));
        		for(String keyCode : branchMap.keySet()){
        			branchOutPw.println(keyCode + ", " + branchMap.get(keyCode ) + ", " + salesMap.get(keyCode));
        		}
        	}finally{
        		branchOutPw.close();  
        	}	
        }catch(IOException e){
        	System.out.println("予期せぬエラーが発生しました"); 
        }	
	}
}
	



    
    










        	
        	



    



    	     
    	

   